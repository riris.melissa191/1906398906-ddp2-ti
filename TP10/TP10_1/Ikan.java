package TP10_1;

public class Ikan extends Hewan {
    private boolean isBeracun;

    public Ikan() {
    }

    public Ikan(String nama, String spesies, boolean isBeracun) {
        this.isBeracun = isBeracun;
        setNama(nama);
        setSpesies(spesies);
    }

    /** Return LevelKeahlian */
    public boolean getisBeracun() {
        return isBeracun;
    }

    /** Set a new LevelKeahlian */
    public void setisBeracun(Boolean isBeracun) {
        this.isBeracun = isBeracun;
    }

    @Override
    public String bernafas() {
        // TODO Auto-generated method stub
        return (this.getNama() + " bernafas dengan insang.");
    }

    @Override
    public String bergerak() {
        // TODO Auto-generated method stub
        return (this.getNama() + " bergerak dengan cara berenang.");
    }

    @Override
    String bersuara() {
        // TODO Auto-generated method stub
        String hasil = "";
        if (this.getisBeracun()){
            hasil = ("Blub blub blub blub. Blub. (Halo, saya " + this.getNama() + ". Saya ikan yang beracun).");
        }
        else{
            hasil = ("Blub blub blub blub. Blub. (Halo, saya " + this.getNama() + ". Saya ikan yang tidak beracun).");
        }
        return hasil;
    }
    
}
