package TP10_1;

import java.util.*;

public class Main {
    
    public static void main(String[] args) {
        ArrayList<Manusia> daftarManusia = new ArrayList<Manusia>();
        ArrayList<Hewan> daftarHewan = new ArrayList<Hewan>();


        Manusia yoga = new Pegawai("Yoga", 100000, "master");
        Pegawai yoga1 = (Pegawai) yoga;
        daftarManusia.add(yoga1);

        Manusia bujang = new Pelanggan("Bujang", 100000);
        Pelanggan bujang1 = (Pelanggan) bujang;
        daftarManusia.add(bujang1);

        Hewan betta = new Ikan("Betta", "Betta Splendens", false);
        Ikan betta1 = (Ikan) betta;
        daftarHewan.add(betta1);

        Ikan betty = new Ikan("Betty", "Betty Splendens", true);
        //Ikan betty1 = (Ikan) betty;
        daftarHewan.add(betty);

        Hewan burhan = new BurungHantu("Burhan", "Bubo Scandiacus");
        BurungHantu burhan1 = (BurungHantu) burhan;
        daftarHewan.add(burhan1);

        Scanner sc = new Scanner(System.in);
        String perintah = "";

        while(!perintah.equalsIgnoreCase("selesai")){
            System.out.print("Silahkan masukkan perintah: ");
            String masukan = sc.nextLine();
            if (!masukan.equalsIgnoreCase("selesai")){
                String[] masukanSplit = masukan.split(" ");
                if (masukanSplit.length == 2){
                    String namanya = masukanSplit[0];
                    String perintahnya = masukanSplit[1];
                    for (int i=0; i<daftarManusia.size();i++){
                        if (namanya.equalsIgnoreCase(daftarManusia.get(i).getNama())){
                            if (daftarManusia.get(i) instanceof Pegawai){
                                if (perintahnya.equalsIgnoreCase("bergerak")){
                                    System.out.println(((Pegawai) daftarManusia.get(i)).bergerak());
                                }
                                else if (perintahnya.equalsIgnoreCase("bekerja")){
                                    System.out.println(((Pegawai) daftarManusia.get(i)).bekerja());
                                }
                                else if (perintahnya.equalsIgnoreCase("bicara")){
                                    System.out.println(((Pegawai) daftarManusia.get(i)).bicara());
                                }
                                else if (perintahnya.equalsIgnoreCase("bernafas")){
                                    System.out.println(((Pegawai) daftarManusia.get(i)).bernafas());
                                }
                                else{
                                    System.out.println("Maaf, Pegawai " + daftarManusia.get(i).getNama() + " tidak bisa " + perintahnya + ".");
                                }
                            }
                            else if (daftarManusia.get(i) instanceof Pelanggan){
                                if (perintahnya.equalsIgnoreCase("bergerak")){
                                    System.out.println(((Pelanggan) daftarManusia.get(i)).bergerak());
                                }
                                else if (perintahnya.equalsIgnoreCase("bicara")){
                                    System.out.println(((Pelanggan) daftarManusia.get(i)).bicara());
                                }
                                else if (perintahnya.equalsIgnoreCase("bernafas")){
                                    System.out.println(((Pelanggan) daftarManusia.get(i)).bernafas());
                                }
                                else if (perintahnya.equalsIgnoreCase("beli")){
                                    System.out.println(((Pelanggan) daftarManusia.get(i)).membeli());
                                }
                                else{
                                    System.out.println("Maaf, Pelanggan " + daftarManusia.get(i).getNama() + " tidak bisa " + perintahnya + ".");
                                }
                            }
                        }
                    }
                    for (int i=0; i<daftarHewan.size();i++){
                        if (namanya.equalsIgnoreCase(daftarHewan.get(i).getNama())){
                            if (daftarHewan.get(i) instanceof Ikan){
                                if (perintahnya.equalsIgnoreCase("bergerak")){
                                    System.out.println(((Ikan) daftarHewan.get(i)).bergerak());
                                }
                                else if (perintahnya.equalsIgnoreCase("bersuara")){
                                    System.out.println(((Ikan) daftarHewan.get(i)).bersuara());
                                }
                                else if (perintahnya.equalsIgnoreCase("bernafas")){
                                    System.out.println(((Ikan) daftarHewan.get(i)).bernafas());
                                }
                                else{
                                    System.out.println("Maaf, Ikan " + daftarHewan.get(i).getSpesies() + " tidak bisa " + perintahnya + ".");
                                }
                            }
                            else if (daftarHewan.get(i) instanceof BurungHantu){
                                if (perintahnya.equalsIgnoreCase("bergerak")){
                                    System.out.println(((BurungHantu) daftarHewan.get(i)).bergerak());
                                }
                                else if (perintahnya.equalsIgnoreCase("bersuara")){
                                    System.out.println(((BurungHantu) daftarHewan.get(i)).bersuara());
                                }
                                else if (perintahnya.equalsIgnoreCase("bernafas")){
                                    System.out.println(((BurungHantu) daftarHewan.get(i)).bernafas());
                                }
                                else{
                                    System.out.println("Maaf, Burung Hantu " + daftarHewan.get(i).getSpesies() + " tidak bisa " + perintahnya + ".");
                                }
                            }
                        }
                    }
                }
                else{
                    System.out.println("Maaf, perintah tidak ditemukan!");
                }
            }
                else{
                    perintah = "selesai";
                    System.out.println("Sampai jumpa!");
                }
        }
    }
}
