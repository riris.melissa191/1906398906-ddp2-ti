package TP10_1;

public abstract class Hewan implements Makhluk {
    private String nama;
    private String spesies;

    public String getNama(){
        return this.nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getSpesies(){
        return this.spesies;
    }

    public void setSpesies(String spesies) {
        this.spesies = spesies;
    }

    @Override
    public String bernafas() {
        // TODO Auto-generated method stub
        return null;
    }

    @Override
    public String bergerak() {
        // TODO Auto-generated method stub
        return null;
    }

    abstract String bersuara();

}