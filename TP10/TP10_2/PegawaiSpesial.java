package TP10_2;

public class PegawaiSpesial extends Pegawai implements bisaLibur{
    public PegawaiSpesial() {
    }

    public PegawaiSpesial(String nama, int uang, String LevelKeahlian) {
        setLevelKeahlian(LevelKeahlian);
        setNama(nama);
        setUang(uang);
    }
    @Override
    public String libur() {
        // TODO Auto-generated method stub
        return (this.getNama() + " sedang berlibur ke Akihabara.");
    }

    @Override
    String bicara() {
        // TODO Auto-generated method stub
        return ("Halo, saya " + this.getNama() + ". Uang saya adalah " + this.getUang() + ", dan level keahlian saya adalah " + this.getLevelKeahlian() +". Saya memiliki privilege yaitu bisa libur.");
    }
    
}
