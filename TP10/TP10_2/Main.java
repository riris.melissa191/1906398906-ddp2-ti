package TP10_2;

import java.util.*;

public class Main {
    
    public static void main(String[] args) {
        ArrayList<Manusia> daftarManusia = new ArrayList<Manusia>();
        ArrayList<Hewan> daftarHewan = new ArrayList<Hewan>();

        Manusia bujang = new Pegawai("Bujang", 100000, "pemula");
        Pegawai bujang1 = (Pegawai) bujang;
        daftarManusia.add(bujang1);

        Manusia yoga = new PegawaiSpesial("Yoga", 100000, "master");
        PegawaiSpesial yoga1 = (PegawaiSpesial) yoga;
        daftarManusia.add(yoga1);

        Hewan kerapu = new Ikan("Kerapu Batik", "Epinephelus Polyphekadion", false);
        Ikan kerapu1 = (Ikan) kerapu;
        daftarHewan.add(kerapu1);

        Hewan ikanTerbang = new IkanSpesial("Ikan Terbang Biru", "Exocoetus volitans", false);
        IkanSpesial ikanTerbang1 = (IkanSpesial) ikanTerbang;
        daftarHewan.add(ikanTerbang1);

        Scanner sc = new Scanner(System.in);
        String perintah = "";

        while(!perintah.equalsIgnoreCase("selesai")){
            System.out.print("Silahkan masukkan perintah: ");
            String masukan = sc.nextLine();
            if (!masukan.equalsIgnoreCase("selesai")){
                String[] masukanSplit = masukan.split(" ");
                List <String> lst = new ArrayList<String>(Arrays.asList(masukanSplit)); 
                int index = lst.size() - 1;
                String perintahnya = lst.get(index);
                lst.remove(index);
                String namanya = String.join(" ", lst);
                for (int i=0; i<daftarManusia.size();i++){
                    if (namanya.equalsIgnoreCase(daftarManusia.get(i).getNama())){
                        if (daftarManusia.get(i) instanceof PegawaiSpesial){
                            if (perintahnya.equalsIgnoreCase("libur")){
                                System.out.println(((PegawaiSpesial) daftarManusia.get(i)).libur());
                            }
                            else if (perintahnya.equalsIgnoreCase("bergerak")){
                                System.out.println(((Pegawai) daftarManusia.get(i)).bergerak());
                            }
                            else if (perintahnya.equalsIgnoreCase("bekerja")){
                                System.out.println(((Pegawai) daftarManusia.get(i)).bekerja());
                            }
                            else if (perintahnya.equalsIgnoreCase("bicara")){
                                System.out.println(((Pegawai) daftarManusia.get(i)).bicara());
                            }
                            else if (perintahnya.equalsIgnoreCase("bernafas")){
                                System.out.println(((Pegawai) daftarManusia.get(i)).bernafas());
                            }
                            else{
                                System.out.println("Maaf, Pegawai " + daftarManusia.get(i).getNama() + " tidak bisa " + perintahnya + ".");
                            }
                        }
                        else if (daftarManusia.get(i) instanceof Pegawai){
                            if (perintahnya.equalsIgnoreCase("bergerak")){
                                System.out.println(((Pegawai) daftarManusia.get(i)).bergerak());
                            }
                            else if (perintahnya.equalsIgnoreCase("bekerja")){
                                System.out.println(((Pegawai) daftarManusia.get(i)).bekerja());
                            }
                            else if (perintahnya.equalsIgnoreCase("bicara")){
                                System.out.println(((Pegawai) daftarManusia.get(i)).bicara());
                            }
                            else if (perintahnya.equalsIgnoreCase("bernafas")){
                                System.out.println(((Pegawai) daftarManusia.get(i)).bernafas());
                            }
                            else{
                                System.out.println("Maaf, Pegawai " + daftarManusia.get(i).getNama() + " tidak bisa " + perintahnya + ".");
                            }
                        }
                        else if (daftarManusia.get(i) instanceof Pelanggan){
                            if (perintahnya.equalsIgnoreCase("bergerak")){
                                System.out.println(((Pelanggan) daftarManusia.get(i)).bergerak());
                            }
                            else if (perintahnya.equalsIgnoreCase("bicara")){
                                System.out.println(((Pelanggan) daftarManusia.get(i)).bicara());
                            }
                            else if (perintahnya.equalsIgnoreCase("bernafas")){
                                System.out.println(((Pelanggan) daftarManusia.get(i)).bernafas());
                            }
                            else if (perintahnya.equalsIgnoreCase("beli")){
                                System.out.println(((Pelanggan) daftarManusia.get(i)).membeli());
                            }
                            else{
                                System.out.println("Maaf, Pelanggan " + daftarManusia.get(i).getNama() + " tidak bisa " + perintahnya + ".");
                            }
                        }
                    }
                }
                for (int i=0; i<daftarHewan.size();i++){
                    if (namanya.equalsIgnoreCase(daftarHewan.get(i).getNama())){
                        if (daftarHewan.get(i) instanceof IkanSpesial){
                            if (perintahnya.equalsIgnoreCase("terbang")){
                                System.out.println(((IkanSpesial) daftarHewan.get(i)).terbang());
                            }
                            else if (perintahnya.equalsIgnoreCase("bergerak")){
                                System.out.println(((Ikan) daftarHewan.get(i)).bergerak());
                            }
                            else if (perintahnya.equalsIgnoreCase("bersuara")){
                                System.out.println(((Ikan) daftarHewan.get(i)).bersuara());
                            }
                            else if (perintahnya.equalsIgnoreCase("bernafas")){
                                System.out.println(((Ikan) daftarHewan.get(i)).bernafas());
                            }
                            else{
                                System.out.println("Maaf, Ikan " + daftarHewan.get(i).getNama() + " tidak bisa " + perintahnya + ".");
                            }
                        }
                        else if (daftarHewan.get(i) instanceof Ikan){
                            if (perintahnya.equalsIgnoreCase("bergerak")){
                                System.out.println(((Ikan) daftarHewan.get(i)).bergerak());
                            }
                            else if (perintahnya.equalsIgnoreCase("bersuara")){
                                System.out.println(((Ikan) daftarHewan.get(i)).bersuara());
                            }
                            else if (perintahnya.equalsIgnoreCase("bernafas")){
                                System.out.println(((Ikan) daftarHewan.get(i)).bernafas());
                            }
                            else{
                                System.out.println("Maaf, Ikan " + daftarHewan.get(i).getSpesies() + " tidak bisa " + perintahnya + ".");
                            }
                        }
                        else if (daftarHewan.get(i) instanceof BurungHantu){
                            if (perintahnya.equalsIgnoreCase("bergerak")){
                                System.out.println(((BurungHantu) daftarHewan.get(i)).bergerak());
                            }
                            else if (perintahnya.equalsIgnoreCase("bersuara")){
                                System.out.println(((BurungHantu) daftarHewan.get(i)).bersuara());
                            }
                            else if (perintahnya.equalsIgnoreCase("bernafas")){
                                System.out.println(((BurungHantu) daftarHewan.get(i)).bernafas());
                            }
                            else{
                                System.out.println("Maaf, Burung Hantu " + daftarHewan.get(i).getSpesies() + " tidak bisa " + perintahnya + ".");
                            }
                        }
                    }
                }
                ArrayList<String> kumpulannama = new ArrayList<String>();
                for (int i=0; i<daftarManusia.size();i++){
                    kumpulannama.add(daftarManusia.get(i).getNama().toLowerCase());
                }
                for (int i=0; i<daftarHewan.size();i++){
                    kumpulannama.add(daftarHewan.get(i).getNama().toLowerCase());
                }
                if (!kumpulannama.contains(namanya)){
                    System.out.println("Maaf, tidak ada makhluk bernama " + namanya +".");
                }
            }else{
                perintah = "selesai";
                System.out.println("Sampai jumpa!");
            }
        }
    }
}
