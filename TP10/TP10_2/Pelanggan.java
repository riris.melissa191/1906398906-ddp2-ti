package TP10_2;

public class Pelanggan extends Manusia {
    public Pelanggan() {
    }

    public Pelanggan(String nama, int uang) {
        setNama(nama);
        setUang(uang);
    }

    public String membeli(){    
        return (this.getNama() + " membeli makanan dan minuman di kedai VoidMain.");
    }

    @Override
    String bicara() {
        // TODO Auto-generated method stub
        return ("Halo, saya " + this.getNama() + ". Uang saya adalah " + this.getUang() + ".");
    }
}

    // @Override
    // public String bergerak() {
    //     // TODO Auto-generated method stub
    //     return null;
    // }

    // @Override
    // public String bernafas() {
    //     // TODO Auto-generated method stub
    //     return null;
    // }