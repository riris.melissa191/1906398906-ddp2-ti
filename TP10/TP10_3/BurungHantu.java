package TP10_3;

public class BurungHantu extends Hewan {
    public BurungHantu() {
    }

    public BurungHantu(String nama, String spesies) {
        setNama(nama);
        setSpesies(spesies);
    }

    @Override
    public String bernafas() {
        // TODO Auto-generated method stub
        return (this.getNama() + " bernafas dengan paru-paru.");
    }

    @Override
    public String bergerak() {
        // TODO Auto-generated method stub
        return (this.getNama() + " bergerak dengan cara terbang.");
    }
    @Override
    String bersuara() {
        // TODO Auto-generated method stub
        return ("Hooooh hoooooooh. (Halo, saya " + this.getNama() + ". Saya adalah burung hantu).");
    }
    
    public String toString() {
        return this.bersuara();
    }
}
