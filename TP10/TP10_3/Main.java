package TP10_3;

import java.util.*;

public class Main {
    
    public static void main(String[] args) {
        ArrayList<Makhluk> daftarMakhluk = new ArrayList<Makhluk>();

        Manusia bujang = new Pegawai("Bujang", 100000, "pemula");
        Pegawai bujang1 = (Pegawai) bujang;
        daftarMakhluk.add(bujang1);

        Manusia yoga = new PegawaiSpesial("Yoga", 100000, "master");
        PegawaiSpesial yoga1 = (PegawaiSpesial) yoga;
        daftarMakhluk.add(yoga1);

        Hewan kerapu = new Ikan("Kerapu Batik", "Epinephelus Polyphekadion", false);
        Ikan kerapu1 = (Ikan) kerapu;
        daftarMakhluk.add(kerapu1);

        Hewan ikanTerbang = new IkanSpesial("Ikan Terbang Biru", "Exocoetus volitans", false);
        IkanSpesial ikanTerbang1 = (IkanSpesial) ikanTerbang;
        daftarMakhluk.add(ikanTerbang1);

        Scanner sc = new Scanner(System.in);
        String perintah = "";

        while(!perintah.equalsIgnoreCase("selesai")){
            System.out.print("Silahkan masukkan perintah: ");
            String masukan = sc.nextLine();
            if (!masukan.equalsIgnoreCase("selesai")){
                String[] masukanSplit = masukan.split(" ");
                List <String> lst = new ArrayList<String>(Arrays.asList(masukanSplit)); 
                String perintahnya = lst.get(0);
                lst.remove(0);
                String namanya = String.join(" ", lst);
                boolean ada = false;
                if (perintahnya.equalsIgnoreCase("panggil")){
                    for (int i=0; i<daftarMakhluk.size(); i++){
                        if (daftarMakhluk.get(i) instanceof Manusia){
                            Manusia makhluk = (Manusia)daftarMakhluk.get(i);
                            if (namanya.equalsIgnoreCase(makhluk.getNama())){
                                System.out.println(makhluk.toString());
                                ada = true;
                            }
                        }
                        else if (daftarMakhluk.get(i) instanceof Hewan){
                            Hewan makhluk = (Hewan)daftarMakhluk.get(i);
                            if (namanya.equalsIgnoreCase(makhluk.getNama())){
                                System.out.println(makhluk.toString());
                                ada = true;
                            }
                        }
                    }
                    if (ada == false){
                        System.out.println("Maaf, " + namanya + " tidak pernah melewati/mampir di kedai.");
                    }
                }
            }else{
                perintah = "selesai";
                System.out.println("Sampai jumpa!");
            }
        }
    }
}