package TP10_4;

public class Pegawai extends Manusia {
    private String LevelKeahlian;

    public Pegawai() {
    }

    public Pegawai(String nama, int uang, String LevelKeahlian) {
        this.LevelKeahlian = LevelKeahlian;
        setNama(nama);
        setUang(uang);
    }

    /** Return LevelKeahlian */
    public String getLevelKeahlian() {
        return LevelKeahlian;
    }

    /** Set a new LevelKeahlian */
    public void setLevelKeahlian(String LevelKeahlian) {
        this.LevelKeahlian = LevelKeahlian;
    }

    public String bekerja(){
        return (this.getNama() + " bekerja di kedai VoidMain.");
    }

    @Override
    String bicara() {
        // TODO Auto-generated method stub
        return ("Halo, saya " + this.getNama() + ". Uang saya adalah " + this.getUang() + ", dan level keahlian saya adalah " + this.getLevelKeahlian() + ".");
    }
    public String toString() {
        return this.bicara() ;
    }
}