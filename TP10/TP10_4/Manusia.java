package TP10_4;

public abstract class Manusia implements Makhluk {
    private String nama;
    private int uang;

    public String getNama(){
        return this.nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public int getUang(){
        return this.uang;
    }

    public void setUang(int uang) {
        this.uang = uang;
    }

    @Override
    public String bernafas() {
        // TODO Auto-generated method stub
        return (this.getNama() + " bernafas dengan menggunakan paru-paru.");
    }

    @Override
    public String bergerak() {
        // TODO Auto-generated method stub
        return (this.getNama() + " bergerak dengan cara berjalan.");
    }

    abstract String bicara();
}