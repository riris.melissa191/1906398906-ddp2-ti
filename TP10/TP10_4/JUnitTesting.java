package TP10_4;

import org.junit.jupiter.api.*;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;

public class JUnitTesting {

    // 1 . Periksa output dari setiap kemungkinan perintah pada contoh eksekusi program.

    @Test
    public void testBergerakPegawai(){
        Manusia yoga = new Pegawai("Yoga", 100000, "master");
        Pegawai yoga1 = (Pegawai) yoga;
        assertEquals("Yoga bergerak dengan cara berjalan.", yoga1.bergerak());
    }

    @Test
    public void testBekerjaPegawai(){
        Manusia yoga = new Pegawai("Yoga", 100000, "master");
        Pegawai yoga1 = (Pegawai) yoga;
        assertEquals("Yoga bekerja di kedai VoidMain.", yoga1.bekerja());
    }

    @Test
    public void testBicaraPegawai(){
        Manusia yoga = new Pegawai("Yoga", 100000, "master");
        Pegawai yoga1 = (Pegawai) yoga;
        assertEquals("Halo, saya Yoga. Uang saya adalah 100000, dan level keahlian saya adalah master.", yoga1.bicara());
    }

    @Test
    public void testBernafasPegawai(){
        Manusia yoga = new Pegawai("Yoga", 100000, "master");
        Pegawai yoga1 = (Pegawai) yoga;
        assertEquals("Yoga bernafas dengan menggunakan paru-paru.", yoga1.bernafas());
    }

    @Test
    public void testBerbicaraPelanggan(){
        Manusia bujang = new Pelanggan("Bujang", 100000);
        Pelanggan bujang1 = (Pelanggan) bujang;
        assertEquals("Halo, saya Bujang. Uang saya adalah 100000.", bujang1.bicara());
    }

    @Test
    public void testBeliPelanggan(){
        Manusia bujang = new Pelanggan("Bujang", 100000);
        Pelanggan bujang1 = (Pelanggan) bujang;
        assertEquals("Bujang membeli makanan dan minuman di kedai VoidMain.", bujang1.membeli());
    }

    @Test
    public void testBersuaraIkan(){
        Hewan betta = new Ikan("Betta", "Betta Splendens", false);
        Ikan betta1 = (Ikan) betta;
        assertEquals("Blub blub blub blub. Blub. (Halo, saya Betta. Saya ikan yang tidak beracun).", betta1.bersuara());
    }

    @Test
    public void testBernafasIkan(){
        Hewan betta = new Ikan("Betta", "Betta Splendens", false);
        Ikan betta1 = (Ikan) betta;
        assertEquals("Betta bernafas dengan insang.", betta1.bernafas());
    }

    @Test
    public void testBergerakIkan(){
        Hewan betta = new Ikan("Betta", "Betta Splendens", false);
        Ikan betta1 = (Ikan) betta;
        assertEquals("Betta bergerak dengan cara berenang.", betta1.bergerak());
    }
    
    @Test
    public void testBernafasBurungHantu(){
        Hewan burhan = new BurungHantu("Burhan", "Bubo Scandiacus");
        BurungHantu burhan1 = (BurungHantu) burhan;
        assertEquals("Burhan bernafas dengan paru-paru.", burhan1.bernafas());
    }

    @Test
    public void testBergerakBurungHantu(){
        Hewan burhan = new BurungHantu("Burhan", "Bubo Scandiacus");
        BurungHantu burhan1 = (BurungHantu) burhan;
        assertEquals("Burhan bergerak dengan cara terbang.", burhan1.bergerak());
    }

    @Test
    public void testBersuaraBurungHantu(){
        Hewan burhan = new BurungHantu("Burhan", "Bubo Scandiacus");
        BurungHantu burhan1 = (BurungHantu) burhan;
        assertEquals("Hooooh hoooooooh. (Halo, saya Burhan. Saya adalah burung hantu).", burhan1.bersuara());
    }

    @Test
    public void testBicaraPegawaiSpesial(){
        Manusia yoga = new PegawaiSpesial("Yoga", 100000, "master");
        PegawaiSpesial yoga1 = (PegawaiSpesial) yoga;
        assertEquals("Halo, saya Yoga. Uang saya adalah 100000, dan level keahlian saya adalah master. Saya memiliki privilege yaitu bisa libur.", yoga1.bicara());
    }

    @Test
    public void testLiburPegawaiSpesial(){
        Manusia yoga = new PegawaiSpesial("Yoga", 100000, "master");
        PegawaiSpesial yoga1 = (PegawaiSpesial) yoga;
        assertEquals("Yoga sedang berlibur ke Akihabara.", yoga1.libur());
    }

    @Test
    public void testTerbangIkanSpesial(){
        Hewan ikanTerbang = new IkanSpesial("Ikan Terbang Biru", "Exocoetus volitans", false);
        IkanSpesial ikanTerbang1 = (IkanSpesial) ikanTerbang;
        assertEquals("Fwooosssshhhhh! Plup.", ikanTerbang1.terbang());
    }

    @Test
    public void testBersuaraIkanSpesial(){
        Hewan ikanTerbang = new IkanSpesial("Ikan Terbang Biru", "Exocoetus volitans", false);
        IkanSpesial ikanTerbang1 = (IkanSpesial) ikanTerbang;
        assertEquals("Blub blub blub blub. Blub. Blub blub blub. (Halo, saya Ikan Terbang Biru. Saya ikan yang tidak beracun. Saya bisa terbang loh.).", ikanTerbang1.bersuara());
    }

    // 2. Periksa field yang dimasukkan kedalam constructor ketika inisiasi objek.
    // Untuk bagian ini, cukup pilih dua subclass untuk dites(Pegawai dan PegawaiSpesial,
    // atau Ikan dan IkanSpesial). 

    @Test
    public void testNamaPegawai(){
        Manusia yoga = new Pegawai("Yoga", 100000, "master");
        Pegawai yoga1 = (Pegawai) yoga;
        assertEquals("Yoga", yoga1.getNama());
    }

    @Test
    public void testNamaPegawaiSpesial(){
        Manusia yoga = new PegawaiSpesial("Yoga", 100000, "master");
        PegawaiSpesial yoga1 = (PegawaiSpesial) yoga;
        assertEquals("Yoga", yoga1.getNama());
    }

    @Test
    public void testUangPegawai(){
        Manusia yoga = new Pegawai("Yoga", 100000, "master");
        Pegawai yoga1 = (Pegawai) yoga;
        assertEquals(100000, yoga1.getUang());
    }
    
    @Test
    public void testUangPegawaiSpesial(){
        Manusia yoga = new PegawaiSpesial("Yoga", 100000, "master");
        PegawaiSpesial yoga1 = (PegawaiSpesial) yoga;
        assertEquals(100000, yoga1.getUang());
    }

    @Test
    public void testKeahlianPegawai(){
        Manusia yoga = new Pegawai("Yoga", 100000, "master");
        Pegawai yoga1 = (Pegawai) yoga;
        assertEquals("master", yoga1.getLevelKeahlian());
    }

    @Test
    public void testKeahlianPegawaiSpesial(){
        Manusia yoga = new PegawaiSpesial("Yoga", 100000, "master");
        PegawaiSpesial yoga1 = (PegawaiSpesial) yoga;
        assertEquals("master", yoga1.getLevelKeahlian());
    }

    //3. Periksa apakah suatu class merupakan subclass dari class yang seharusnya
    //(Misal, pegawai merupakan subclass dari manusia, dan pegawaiSpesial
    //merupakan subclass dari pegawai DAN manusia).

    @Test
    public void testPegawaiSubClassManusia(){
        Manusia yoga = new Pegawai("Yoga", 100000, "master");
        Pegawai yoga1 = (Pegawai) yoga;
        assertEquals(true, yoga1 instanceof Manusia);
    }

    @Test
    public void testPegawaiSpesialSubClassPegawai(){
        Manusia yoga = new PegawaiSpesial("Yoga", 100000, "master");
        PegawaiSpesial yoga1 = (PegawaiSpesial) yoga;
        assertEquals(true, yoga1 instanceof Pegawai);
    }

    @Test
    public void testPegawaiSpesialSubClassManusia(){
        Manusia yoga = new PegawaiSpesial("Yoga", 100000, "master");
        PegawaiSpesial yoga1 = (PegawaiSpesial) yoga;
        assertEquals(true, yoga1 instanceof Manusia);
    }

    @Test
    public void testIkanSubClassHewan(){
        Hewan betta = new Ikan("Betta", "Betta Splendens", false);
        Ikan betta1 = (Ikan) betta;
        assertEquals(true, betta1 instanceof Hewan);
    }

    @Test
    public void testIkanSpesialSubClassHewan(){
        Hewan betta = new IkanSpesial("Betta", "Betta Splendens", false);
        IkanSpesial betta1 = (IkanSpesial) betta;
        assertEquals(true, betta1 instanceof Hewan);
    }

    @Test
    public void testIkanSpesialSubClassIkan(){
        Hewan betta = new IkanSpesial("Betta", "Betta Splendens", false);
        IkanSpesial betta1 = (IkanSpesial) betta;
        assertEquals(true, betta1 instanceof Ikan);
    }

    //4. Periksa apakah subclass yang dipilih pada poin 2 mengimplementasikan interface
    //yang harusnya diimplementasikan(Misal, pegawai mengimplementasikan Makhluk saja,
    //sedangkan pegawaiSpesial mengimplementasikan Makhluk dan bisaLibur).

    @Test
    public void testPegawaiImplementsMakhluk(){
        Manusia yoga = new Pegawai("Yoga", 100000, "master");
        Pegawai yoga1 = (Pegawai) yoga;
        assertEquals(true, yoga1 instanceof Makhluk);
    }

    @Test
    public void testPegawaiNotImplementsbisaLibur(){
        Manusia yoga = new Pegawai("Yoga", 100000, "master");
        Pegawai yoga1 = (Pegawai) yoga;
        assertEquals(false, yoga1 instanceof bisaLibur);
    }
    @Test
    public void testPegawaiNotImplementsbisaTerbang(){
        Manusia yoga = new Pegawai("Yoga", 100000, "master");
        Pegawai yoga1 = (Pegawai) yoga;
        assertEquals(false, yoga1 instanceof bisaTerbang);
    }

    @Test
    public void testPegawaiSpesialImplementsMakhluk(){
        Manusia yoga = new PegawaiSpesial("Yoga", 100000, "master");
        PegawaiSpesial yoga1 = (PegawaiSpesial) yoga;
        assertEquals(true, yoga1 instanceof Makhluk);
    }

    @Test
    public void testPegawaiSpesialImplementsbisaLibur(){
        Manusia yoga = new PegawaiSpesial("Yoga", 100000, "master");
        PegawaiSpesial yoga1 = (PegawaiSpesial) yoga;
        assertEquals(true, yoga1 instanceof bisaLibur);
    }

    @Test
    public void testPegawaiSpesialNotImplementsbisaTerbang(){
        Manusia yoga = new PegawaiSpesial("Yoga", 100000, "master");
        PegawaiSpesial yoga1 = (PegawaiSpesial) yoga;
        assertEquals(false, yoga1 instanceof bisaTerbang);
    }

    @Test
    public void testIkanImplementsMakhluk(){
        Hewan betta = new Ikan("Betta", "Betta Splendens", false);
        Ikan betta1 = (Ikan) betta;
        assertEquals(true, betta1 instanceof Makhluk);
    }

    @Test
    public void testIkanNotImplementsbisaTerbang(){
        Hewan betta = new Ikan("Betta", "Betta Splendens", false);
        Ikan betta1 = (Ikan) betta;
        assertEquals(false, betta1 instanceof bisaTerbang);
    }

    @Test
    public void testIkanNotImplementsbisaLibur(){
        Hewan betta = new Ikan("Betta", "Betta Splendens", false);
        Ikan betta1 = (Ikan) betta;
        assertEquals(false, betta1 instanceof bisaLibur);
    }

    @Test
    public void testIkanSpesialImplementsMakhluk(){
        Hewan betta = new IkanSpesial("Betta", "Betta Splendens", false);
        IkanSpesial betta1 = (IkanSpesial) betta;
        assertEquals(true, betta1 instanceof Makhluk);
    }

    @Test
    public void testIkanSpesialImplementsbisaTerbang(){
        Hewan betta = new IkanSpesial("Betta", "Betta Splendens", false);
        IkanSpesial betta1 = (IkanSpesial) betta;
        assertEquals(true, betta1 instanceof bisaTerbang);
    }
    @Test
    public void testIkanSpesialNotImplementsbisaLibur(){
        Hewan betta = new IkanSpesial("Betta", "Betta Splendens", false);
        IkanSpesial betta1 = (IkanSpesial) betta;
        assertEquals(false, betta1 instanceof bisaLibur);
    }

}