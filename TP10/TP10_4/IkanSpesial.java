package TP10_4;

public class IkanSpesial extends Ikan implements bisaTerbang{
    public IkanSpesial() {
    }

    public IkanSpesial(String nama, String spesies, boolean isBeracun) {
        setisBeracun(isBeracun);
        setNama(nama);
        setSpesies(spesies);
    }

    @Override
    public String terbang() {
        // TODO Auto-generated method stub
        return ("Fwooosssshhhhh! Plup.");
    }

    @Override
    String bersuara() {
        // TODO Auto-generated method stub
        String hasil = "";
        if (this.getisBeracun()){
            hasil = ("Blub blub blub blub. Blub. Blub blub blub. (Halo, saya " + this.getNama() + ". Saya ikan yang beracun. Saya bisa terbang loh.).");
        }
        else{
            hasil = ("Blub blub blub blub. Blub. Blub blub blub. (Halo, saya " + this.getNama() + ". Saya ikan yang tidak beracun. Saya bisa terbang loh.).");
        }
        return hasil;
    }

    public String toString() {
        return this.bersuara();
    }
}
