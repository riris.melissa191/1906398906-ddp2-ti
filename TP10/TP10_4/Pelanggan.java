package TP10_4;

public class Pelanggan extends Manusia {
    public Pelanggan() {
    }

    public Pelanggan(String nama, int uang) {
        setNama(nama);
        setUang(uang);
    }

    public String membeli(){    
        return (this.getNama() + " membeli makanan dan minuman di kedai VoidMain.");
    }

    @Override
    String bicara() {
        // TODO Auto-generated method stub
        return ("Halo, saya " + this.getNama() + ". Uang saya adalah " + this.getUang() + ".");
    }

    public String toString() {
        return this.bicara() ;
    }
}