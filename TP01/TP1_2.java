import java.util.Scanner;
public class TP1_2 {
    public static void main(String[] args) {
        System.out.println("Selamat datang di DDP2");
        Scanner sc = new Scanner(System.in);
        System.out.print("Nama fakultas apa yang anda ingin ketahui?: ");
        String fakultassingkatan = sc.nextLine(); 
        String fakultas = fakultassingkatan.toLowerCase();
        if (fakultas.equals("fk")){
            System.out.println("Fakultas Kedokteran");
        }
        else if (fakultas.equals("feb")){
            System.out.println("Fakultas Ekonomi & Bisnis");
        }
        else if (fakultas.equals("fh")){
            System.out.println("Fakultas Hukum");
        }




        switch (fakultas) {
            case "fk":
                System.out.println("Fakultas Kedokteran");
                break;            
            case "feb":
                System.out.println("Fakultas Ekonomi & Bisnis");
                break;
            case "fh":
                System.out.println("Fakultas Hukum");
                break;
            case "ft":
                System.out.println("Fakultas Teknik");
                break;
            case "ff":
                System.out.println("Fakultas Farmasi");
                break;
            case "fpsi":
                System.out.println("Fakultas Psikologi");
                break;   
            case "fasilkom":
                System.out.println("Fakultas Ilmu Komputer");
                break;  
            case "fkg":
                System.out.println("Fakultas Kedokteran Gigi");
                break;    
            case "fia":
                System.out.println("Fakultas Ilmu Administrasi");
                break;                                                            
            case "fik":
                System.out.println("Fakultas Ilmu Keperawatan");
                break;
            case "fkm":
                System.out.println("Fakultas Kesehatan Masyarakat");
                break;
            case "fib":
                System.out.println("Fakultas Ilmu Budaya");
                break;
            case "fmipa":
                System.out.println("Fakultas Matematika & Ilmu Pengetahuan Alam");
                break;
            default:
                System.out.println("Fakultas tidak ditemukan");
            }
        sc.close();
    }
}