import java.util.Scanner;
import java.lang.Math; 

public class TP1 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        
        System.out.print("Masukkan kata: ");
        String kata = sc.nextLine(); 
        System.out.print("Masukkan angka: ");
        int angka = sc.nextInt(); 

        if (Math.pow(angka, 2) == 4){
            System.out.println("Angka bernilai 2");
        }
        else if (angka % 2 == 0){
            System.out.println("Angka genap");
        }
        else{
            System.out.println("Bukan genap dan bukan 2");
        }
        
        int length = kata.length();

        if (length < 5){
            System.out.println("Kata yang anda masukan adalah " + kata + " ");
            System.out.println("Panjang katanya kurang dari 5");
        }
        else if (length > 22){
            System.out.println("Kata yang anda masukan sangat panjang");
        }
        else{
            System.out.println("Kata yang anda masukkan biasa saja");
        }
        
        sc.close();

    }
    
}