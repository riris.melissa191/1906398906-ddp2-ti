# 💻 Tugas Pemrograman 1 (Individu)

[![License](https://img.shields.io/badge/License-BSD%203--Clause-blue.svg)](https://opensource.org/licenses/BSD-3-Clause) [![](https://img.shields.io/badge/SCeLE-Click%20Here-orange.svg)](https://scele.cs.ui.ac.id/course/view.php?id=3019) [![](https://img.shields.io/badge/LINE%20OA-Kak%20Burhan-brightgreen)](https://line.me/R/ti/p/%40157zmdwf)

**Nama**: Riris Melissa Winona Togatorop | **NPM**: 1906398906 | **Kelas**: J | **Kode Tutor**: 

## Topik

Hello World, Java, and Git 😄

## Dokumen Tugas

[https://docs.google.com/document/d/1LkeDKrLc-QEXIyGeneWCLZoDnnad1V-h-Jw2_waaH_U/edit?usp=sharing](https://docs.google.com/document/d/1LkeDKrLc-QEXIyGeneWCLZoDnnad1V-h-Jw2_waaH_U/edit?usp=sharing)

## *Checklist*

- [ ] Membaca dan memahami Soal Tugas Pemrograman 1 dengan seksama
- [ ] Membuat program soal 1 di TP1.java
- [ ] Membuat program soal 2 di TP1_2.java
- [ ] Mengumpulkan hasil pengerjaan ke GitLab
- [ ] Melakukan peragaan Tugas Pemrograman ke Tutor