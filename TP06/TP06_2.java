import java.util.Scanner;
import java.util.ArrayList;

public class TP06_2 {
        public static void main(String[] args){
        Scanner scan = new Scanner(System.in);
        int jumlah = scan.nextInt();
        ArrayList<Karyawan> listKaryawan = new ArrayList<Karyawan>(); 
        for (int i=0 ; i < jumlah ; i++){
            Karyawan data = new Karyawan(scan.next(), scan.nextInt(), scan.nextInt());
            listKaryawan.add(data);
            System.out.println(listKaryawan.get(i).getGaji());
        }
        System.out.print("Rata-rata gaji karyawan adalah ");
        System.out.printf("%.2f",rerataGaji(listKaryawan));
        System.out.println("\nKaryawan dengan gaji tertinggi adalah " + gajiTertinggi(listKaryawan));
        System.out.println("Karyawan dengan gaji terendah adalah " + gajiTerendah(listKaryawan));
        System.out.println("Karyawan dengan umur tertinggi adalah " + umurTertinggi(listKaryawan));
        System.out.println("Karyawan dengan umur terendah adalah " + umurTerendah(listKaryawan));
        System.out.println("Rata-rata umur karyawan adalah " + rataRataUmur(listKaryawan));
    }

    //TODO
    // private static DecimalFormat df2 = new DecimalFormat("#.##");
    public static double rerataGaji( ArrayList<Karyawan> listKaryawan) {
        int panjang = listKaryawan.size();
        double total = 0;
        for (int i=0 ; i<panjang ; i++){
            total += listKaryawan.get(i).getGaji();
            }
        double rerata = total/panjang;
        return rerata;
    }

    //TODO
    //Jangan menggunakan method sort pada array
    public static String gajiTerendah( ArrayList<Karyawan> listKaryawan) {
        int panjang = listKaryawan.size();
        double min = listKaryawan.get(0).getGaji();
        String min2 = listKaryawan.get(0).getNama();
        for (int i=1 ; i<panjang ; i++){
            if (listKaryawan.get(i).getGaji() < min){
                min = listKaryawan.get(i).getGaji();
                min2 = listKaryawan.get(i).getNama();
            }
        }
        return min2;
    }

    //TODO
    //Jangan menggunakan method sort pada array
    public static String gajiTertinggi( ArrayList<Karyawan> listKaryawan) {
        int panjang = listKaryawan.size();
        double max = listKaryawan.get(0).getGaji();
        String max2 = listKaryawan.get(0).getNama();
        for (int i=1 ; i<panjang ; i++){
            if (listKaryawan.get(i).getGaji() > max){
                max = listKaryawan.get(i).getGaji();
                max2 = listKaryawan.get(i).getNama();
            }
        }
        return max2;
    }

    public static String umurTertinggi( ArrayList<Karyawan> listKaryawan) {
        int panjang = listKaryawan.size();
        int max = listKaryawan.get(0).getUmur();
        String max2 = listKaryawan.get(0).getNama();
        for (int i=1 ; i<panjang ; i++){
            if (listKaryawan.get(i).getUmur() > max){
                max = listKaryawan.get(i).getUmur();
                max2 = listKaryawan.get(i).getNama();
            }
        }
        return max2;
    }

    public static String umurTerendah( ArrayList<Karyawan> listKaryawan) {
        int panjang = listKaryawan.size();
        int min = listKaryawan.get(0).getUmur();
        String min2 = listKaryawan.get(0).getNama();
        for (int i=1 ; i<panjang ; i++){
            if (listKaryawan.get(i).getUmur() < min){
                min = listKaryawan.get(i).getUmur();
                min2 = listKaryawan.get(i).getNama();
            }
        }
        return min2;
    }

    public static int rataRataUmur( ArrayList<Karyawan> listKaryawan) {
        int panjang = listKaryawan.size();
        int total = 0;
        for (int i=0 ; i<panjang ; i++){
            total += listKaryawan.get(i).getUmur();
        }
        int rataRata = total/panjang;
        return rataRata;
    }

}
