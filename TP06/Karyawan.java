public class Karyawan {
    private String nama;
    private int umur;
    private int lamaBekerja;
    private double gaji;

    //TODO
    public Karyawan(String nama, int umur, int lamaBekerja) {
        this.nama = nama;
        this.umur = umur;
        this.lamaBekerja = lamaBekerja;
    }

    public Karyawan(){
    }

    //TODO
    public String getNama(){
        return this.nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public int getUmur(){
        return this.umur;
    }

    public void setUmur(int umur) {
        this.umur = umur;
    }

    public int getlamaBekerja(){
        return this.lamaBekerja;
    }

    public void setlamaBekerja(int lamaBekerja) {
        this.lamaBekerja = lamaBekerja;
    }

    public double getGaji(){
        double gaji = 100;
        if (this.umur <= 40){
            for (int i=3 ; i<=lamaBekerja ; i+=3){
                gaji = (gaji*105/100);
                }
            }
        else {
            for (int i=3 ; i<=lamaBekerja ; i+=3){
                gaji = (gaji*105/100);
                }
            gaji += 10;
            }
        return gaji;
    }

    public void setGaji(double gaji) {
        this.gaji = gaji;
    }

}
