package TP12_2;

import java.util.*;

class Mahasiswa extends Manusia{
    private String npm;
    private MataKuliah [] matkul;

    public Mahasiswa(String nama, String npm) {
        super(nama);
        // TODO Auto-generated constructor stub
        this.npm = npm;
        this.matkul = new MataKuliah [0];
    }

    public String getNPM(){
        return this.npm;
    }

    public void setNPM(String npm) {
        this.npm = npm;
    }


    public MataKuliah[] getMatkul() {
        return this.matkul;
    }

    public void setMatkul(MataKuliah[] matkul) {
        this.matkul = matkul;
    }

    public void tambahMatKul(MataKuliah matakuliah){
        List<MataKuliah> matkulList = Arrays.asList(this.matkul);
        if (!matkulList.contains(matakuliah)){
            this.matkul = Arrays.copyOf(this.matkul, this.matkul.length + 1);
            this.matkul[this.matkul.length - 1] = matakuliah;
            matakuliah.tambahMhs(this);
        }
    }

    public void dropMatKul(MataKuliah matakuliah){
        MataKuliah [] matkulbaru = new MataKuliah[0];
        if (Arrays.asList(this.matkul).contains(matakuliah)){
            for (int i=0; i<this.matkul.length; i++){
                if (this.matkul[i] != matakuliah){
                    matkulbaru = Arrays.copyOf(matkulbaru, matkulbaru.length + 1);
                    matkulbaru[matkulbaru.length - 1] = matakuliah;
                    for (int j=0; j<matkulbaru.length;j++){
                        matkulbaru[j] = this.matkul[i];
                    }
                }
            }
            this.matkul = Arrays.copyOf(this.matkul, this.matkul.length - 1);
            this.matkul = matkulbaru;
        }
    }

    @Override
    public String toString(){
        return this.getNama() + "," + this.getNPM() + "\n";
    }
    
}
