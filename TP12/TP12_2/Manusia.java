package TP12_2;

abstract class Manusia{
    private String nama;

    public Manusia(String nama){
        this.nama = nama;
    }

    public String getNama(){
        return this.nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }
    
    public abstract String toString();
}