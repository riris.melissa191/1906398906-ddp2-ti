package TP12_2;

import java.util.*;

class Dosen extends Manusia{
    private String nip;
    private MataKuliah [] matkul;

    public Dosen(String nama, String nip) {
        super(nama);
        // TODO Auto-generated constructor stub
        this.nip = nip;
        this.matkul = new MataKuliah [0];
    }

    public String getNIP(){
        return this.nip;
    }

    public void setNIP(String nip) {
        this.nip = nip;
    }


    public MataKuliah[] getMatkul() {
        return this.matkul;
    }

    public void setMatkul(MataKuliah[] matkul) {
        this.matkul = matkul;
    }

    public void assignMatkul(MataKuliah matakuliah){
        int count = 0;
        List<MataKuliah> matkulList = Arrays.asList(this.matkul);
        if (!matkulList.contains(matakuliah) && count <= 2){
            this.matkul = Arrays.copyOf(this.matkul, this.matkul.length + 1);
            this.matkul[this.matkul.length - 1] = matakuliah;
            matakuliah.assignDosen(this);
            count++;
        }
        else if (count > 2){
            System.out.println("Dosen hanya bisa mengajar maksimal 2 mata kuliah.");
        }
    }

    @Override
    public String toString(){
        return this.getNama() + "," + this.getNIP() + "\n"; 
    }
    
}
