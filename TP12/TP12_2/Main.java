package TP12_2;

public class Main {
    public static void main(String[] args) {
        Mahasiswa riris = new Mahasiswa("Riris", "1906398906");
        Mahasiswa ebay = new Mahasiswa("Eba", "19063930206");
        MataKuliah ddp = new MataKuliah("DDP2", "CSGE601021");
        MataKuliah coba = ddp.bacaBerkas("contoh.txt");
        Dosen ade = new Dosen("Ade Azurat", "10293639460"); 

        ddp.tambahMhs(riris);
        ddp.tambahMhs(ebay);
        ddp.assignDosen(ade);
        // System.out.println(riris.toString());
        // System.out.println(ddp.toString());
        // System.out.println(ade.toString());

        ddp.buatBerkas("baru.txt");
    }
}
