package TP12_2;

import java.io.*;
import java.util.*;

class MataKuliah {
    private String nama;
    private String kode;
    private Mahasiswa[] mahasiswa;
    private Dosen[] dosen;

    public MataKuliah(String nama, String kode) {
        this.nama = nama;
        this.kode = kode;
        this.mahasiswa = new Mahasiswa[0];
        this.dosen = new Dosen[0];
    }

    public String getNama() {
        return this.nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getKode() {
        return this.kode;
    }

    public void setKode(String kode) {
        this.kode = kode;
    }

    public Mahasiswa[] getMahasiswa() {
        return this.mahasiswa;
    }

    public void setMahasiswa(Mahasiswa[] mahasiswa) {
        this.mahasiswa = mahasiswa;
    }

    public Dosen[] getDosen() {
        return this.dosen;
    }

    public void setDosen(Dosen[] dosen) {
        this.dosen = dosen;
    }

    public void tambahMhs(Mahasiswa mhs) {
        List<Mahasiswa> mhsList = Arrays.asList(this.mahasiswa);
        if (!mhsList.contains(mhs)) {
            this.mahasiswa = Arrays.copyOf(this.mahasiswa, this.mahasiswa.length + 1);
            this.mahasiswa[this.mahasiswa.length - 1] = mhs;
            mhs.tambahMatKul(this);
        }
    }

    public void dropMhs(Mahasiswa mhs) {
        Mahasiswa[] mhsbaru = new Mahasiswa[0];
        if (Arrays.asList(this.mahasiswa).contains(mhs)) {
            for (int i = 0; i < this.mahasiswa.length; i++) {
                if (this.mahasiswa[i] != mhs) {
                    mhsbaru = Arrays.copyOf(mhsbaru, mhsbaru.length + 1);
                    mhsbaru[mhsbaru.length - 1] = mhs;
                    for (int j = 0; j < mhsbaru.length; j++) {
                        mhsbaru[j] = this.mahasiswa[i];
                    }
                }
            }
            this.mahasiswa = Arrays.copyOf(this.mahasiswa, this.mahasiswa.length - 1);
            this.mahasiswa = mhsbaru;
        }
    }

    public void assignDosen(Dosen dsn) {
        List<Dosen> dosenList = Arrays.asList(this.dosen);
        int count = 0;
        if (!dosenList.contains(dsn) && count <= 4) {
            this.dosen = Arrays.copyOf(this.dosen, this.dosen.length + 1);
            this.dosen[this.dosen.length - 1] = dsn;
            dsn.assignMatkul(this);
            count++;
        }
    }

    public String toString() {
        return this.getNama() + "," + this.getKode() + "\n";
    }

    public String getkodeMatkul() {
        String klasifikasi = "";
        if (this.getKode().startsWith("UIGE")) {
            klasifikasi = "Mata Kuliah Wajib Universitas";
        } else if (this.getKode().startsWith("UIST")) {
            klasifikasi = "Mata Kuliah Wajib Rumpun Sains dan Teknologi";
        } else if (this.getKode().startsWith("CSGE")) {
            klasifikasi = "Mata Kuliah Wajib Fakultas";
        } else if (this.getKode().startsWith("CSCM")) {
            klasifikasi = "Mata Kuliah Wajib Program Studi Ilmu Komputer";
        } else if (this.getKode().startsWith("CSIM")) {
            klasifikasi = "Mata Kuliah Wajib Program Studi Sistem Informasi";
        } else if (this.getKode().startsWith("CSCE")) {
            klasifikasi = "Mata Kuliah Peminatan Program Studi Ilmu Komputer";
        } else if (this.getKode().startsWith("CSIE")) {
            klasifikasi = "Mata Kuliah Peminatan Program Studi Sistem Informasi";
        }
        return klasifikasi;
    }

    public MataKuliah bacaBerkas(String filename) {
        Scanner file;
        MataKuliah matkul = new MataKuliah("", "");
        try {
            file = new Scanner(new File(filename));
            while (file.hasNextLine()) {
                String[] arr = file.nextLine().split(",");
                try {
                    int kode = Integer.parseInt(arr[0].substring(arr[0].length() - 1));
                } catch (NumberFormatException e) {
                    Dosen dsn = new Dosen(arr[1], arr[0]);
                    dsn.assignMatkul(matkul);
                }
                try {
                    int kode = Integer.parseInt(arr[0].substring(0, 4));
                } catch (NumberFormatException e) {
                    // TODO: handle exception
                    matkul = new MataKuliah(arr[1], arr[0]);
                }
                try {
                    int kode = Integer.parseInt(arr[0]);
                    Mahasiswa mhs = new Mahasiswa(arr[1], arr[0]);
                    mhs.tambahMatKul(matkul);
                } catch (Exception e) {
                }
            }
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return matkul;
    }

    public void buatBerkas(String filename) {
        try {
            PrintWriter pw = new PrintWriter(new File(filename));
            pw.write(this.toString());
            for (int i=0; i<mahasiswa.length; i++){
                pw.write(mahasiswa[i].toString());
            }
            for (int i=0; i<dosen.length; i++){
                pw.write(dosen[i].toString());
            }
            pw.flush();
            pw.close();
        } catch (FileNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

}
