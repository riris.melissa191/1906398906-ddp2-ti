package TP12_1;

import java.util.*;

class Mahasiswa {
    private String nama;
    private String npm;
    private MataKuliah [] matkul;

    public Mahasiswa(String nama, String npm){
        this.nama = nama;
        this.npm = npm;
        this.matkul = new MataKuliah [0];
    }

    public String getNama(){
        return this.nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getNPM(){
        return this.npm;
    }

    public void setNPM(String npm) {
        this.npm = npm;
    }


    public MataKuliah[] getMatkul() {
        return this.matkul;
    }

    public void setMatkul(MataKuliah[] matkul) {
        this.matkul = matkul;
    }

    public void tambahMatKul(MataKuliah matakuliah){
        List<MataKuliah> matkulList = Arrays.asList(this.matkul);
        if (!matkulList.contains(matakuliah)){
            this.matkul = Arrays.copyOf(this.matkul, this.matkul.length + 1);
            this.matkul[this.matkul.length - 1] = matakuliah;
            matakuliah.tambahMhs(this);
        }
        else{
            System.out.println("Mata kuliah sudah ditambahkan");
        }
    }

    public void dropMatKul(MataKuliah matakuliah){
        MataKuliah [] matkulbaru = new MataKuliah[0];
        if (Arrays.asList(this.matkul).contains(matakuliah)){
            for (int i=0; i<this.matkul.length; i++){
                if (this.matkul[i] != matakuliah){
                    matkulbaru = Arrays.copyOf(matkulbaru, matkulbaru.length + 1);
                    matkulbaru[matkulbaru.length - 1] = matakuliah;
                    for (int j=0; j<matkulbaru.length;j++){
                        matkulbaru[j] = this.matkul[i];
                    }
                }
            }
            this.matkul = Arrays.copyOf(this.matkul, this.matkul.length - 1);
            this.matkul = matkulbaru;
        }
    }

    public String toString(){
        if (this.getMatkul().length != 0){
            String daftarMatkul= this.getNama() + "-" + this.getNPM() +" mengambil mata kuliah:\n";
            for (int i=0; i<this.matkul.length; i++){
                daftarMatkul += this.matkul[i].getNama() + "-";
                daftarMatkul += this.matkul[i].getKode() + "\n";
            }
            return daftarMatkul;
        }
        return this.getNama() + " tidak mengambil mata kuliah apapun!";
    }
}