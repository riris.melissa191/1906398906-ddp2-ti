package TP12_1;

import java.util.*;

class MataKuliah {
    private String nama;
    private String kode;
    private Mahasiswa [] mahasiswa;

    public MataKuliah(String nama, String kode){
        this.nama = nama;
        this.kode = kode;
        this.mahasiswa = new Mahasiswa [0];
    }

    public String getNama(){
        return this.nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public String getKode(){
        return this.kode;
    }

    public void setKode(String kode) {
        this.kode = kode;
    }

    public Mahasiswa[] getMahasiswa() {
        return this.mahasiswa;
    }

    public void setMahasiswa( Mahasiswa [] mahasiswa) {
        this.mahasiswa = mahasiswa;
    }

    public void tambahMhs(Mahasiswa mhs){
        List<Mahasiswa> mhsList = Arrays.asList(this.mahasiswa);
        if (!mhsList.contains(mhs)){
            this.mahasiswa = Arrays.copyOf(this.mahasiswa, this.mahasiswa.length + 1);
            this.mahasiswa[this.mahasiswa.length - 1] = mhs;
            mhs.tambahMatKul(this);
        }
        else{
            System.out.println("Mahasiswa sudah ditambahkan");
        }
    }

    public void dropMhs(Mahasiswa mhs){
        Mahasiswa [] mhsbaru = new Mahasiswa[0];
        if (Arrays.asList(this.mahasiswa).contains(mhs)){
            for (int i=0; i<this.mahasiswa.length; i++){
                if (this.mahasiswa[i] != mhs){
                    mhsbaru = Arrays.copyOf(mhsbaru, mhsbaru.length + 1);
                    mhsbaru[mhsbaru.length - 1] = mhs;
                    for (int j=0; j<mhsbaru.length;j++){
                        mhsbaru[j] = this.mahasiswa[i];
                    }
                }
            }
            this.mahasiswa = Arrays.copyOf(this.mahasiswa, this.mahasiswa.length - 1);
            this.mahasiswa = mhsbaru;
        }
    }

    public String toString(){
        if (this.getMahasiswa().length != 0){
            String daftarMahasiswa= this.getNama() + "-" + this.getKode() +" diambil mahasiswa:\n";
            for (int i=0; i<this.mahasiswa.length; i++){
                daftarMahasiswa += this.mahasiswa[i].getNama() + "-";
                daftarMahasiswa += this.mahasiswa[i].getNPM() + "\n";
            }
            return daftarMahasiswa;
        }
        return this.getNama() + " tidak diambil siapapun!";
    }

    public String getkodeMatkul(){
        String klasifikasi = "";
        if (this.getKode().startsWith("UIGE")){
            klasifikasi = "Mata Kuliah Wajib Universitas";
        }
        else if (this.getKode().startsWith("UIST")){
            klasifikasi = "Mata Kuliah Wajib Rumpun Sains dan Teknologi";
        }
        else if (this.getKode().startsWith("CSGE")){
            klasifikasi = "Mata Kuliah Wajib Fakultas";
        }
        else if (this.getKode().startsWith("CSCM")){
            klasifikasi = "Mata Kuliah Wajib Program Studi Ilmu Komputer";
        }
        else if (this.getKode().startsWith("CSIM")){
            klasifikasi = "Mata Kuliah Wajib Program Studi Sistem Informasi";
        }
        else if (this.getKode().startsWith("CSCE")){
            klasifikasi = "Mata Kuliah Peminatan Program Studi Ilmu Komputer";
        }
        else if (this.getKode().startsWith("CSIE")){
            klasifikasi = "Mata Kuliah Peminatan Program Studi Sistem Informasi";
        }
        return klasifikasi;
    }


}
