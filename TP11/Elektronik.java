class Elektronik extends Barang {
    private String jenis;
    private String kondisi;

    public Elektronik() {
    }

    public Elektronik(String jenis, String kondisi) {
        this.jenis = jenis;
        this.kondisi = kondisi;
    }

    public String getJenis() {
        return jenis;
    }

    /** Set a new jenis */
    public void setJenis(String jenis) {
        this.jenis = jenis;
    }

    public String getKondisi() {
        return kondisi;
    }

    /** Set a new kondisi */
    public void setKondisi(String kondisi) {
        this.kondisi = kondisi;
    }
    
    public double getValue(){
        double value = 0.0;
        if (this.jenis.equalsIgnoreCase("laptop")){
            value = 500.0;
        }
        else if (this.jenis.equalsIgnoreCase("hp")){
            value = 200.0;
        }
        else if (this.jenis.equalsIgnoreCase("modem")){
            value = 100.0;
        }
        if (this.kondisi.equalsIgnoreCase("buruk")){
            value = value*0.25;
        }
        else if (this.kondisi.equalsIgnoreCase("menengah")){
            value = value*0.80;
        }
        else if (this.kondisi.equalsIgnoreCase("baik")){
            value = value*1;
        }
        else if (this.kondisi.equalsIgnoreCase("baru")){
            value = value*1.25;
        }
        return value;
    }

    public String toString() {
        return this.jenis + " " + this.kondisi;
    }
}