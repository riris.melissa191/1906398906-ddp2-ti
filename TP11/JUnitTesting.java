import org.junit.jupiter.api.*;

import static org.junit.Assert.assertEquals;
import static org.junit.jupiter.api.Assertions.*;

public class JUnitTesting {

    // 1 . Periksa output dari setiap kemungkinan perintah pada contoh eksekusi program.

    @Test
    public void TestgetValueHPBuruk(){
        Barang HPBuruk = new Elektronik("HP", "Buruk");
        Elektronik HPBuruk1 = (Elektronik) HPBuruk;
        assertEquals("50.0", Double.toString(HPBuruk1.getValue()));
    }

    @Test
    public void TestgetValueModemMenengah(){
        Barang ModemMenengah = new Elektronik("Modem", "Menengah");
        Elektronik ModemMenengah1 = (Elektronik) ModemMenengah;
        assertEquals("80.0", Double.toString(ModemMenengah1.getValue()));
    }

    @Test
    public void TestgetLBiru(){
        Barang LBiru = new Pakaian('L', "Biru");
        Pakaian LBiru1 = (Pakaian) LBiru;
        assertEquals("40.0", Double.toString(LBiru1.getValue()));
    }

    @Test
    public void TestgetTotalValue(){
        Kardus <Elektronik> Sierra = new Kardus<Elektronik>();
        Barang HPBuruk = new Elektronik("HP", "Buruk");
        Elektronik HPBuruk1 = (Elektronik) HPBuruk;
        Barang ModemMenengah = new Elektronik("Modem", "Menengah");
        Elektronik ModemMenengah1 = (Elektronik) ModemMenengah;
        Sierra.tambahBarang(HPBuruk1);
        Sierra.tambahBarang(ModemMenengah1);
        assertEquals("130.0", Double.toString(Sierra.getTotalValue()));
    }

    @Test
    public void rekapElektronik(){
        Kardus <Elektronik> Sierra = new Kardus<Elektronik>();
        Barang HPBuruk = new Elektronik("HP", "Buruk");
        Elektronik HPBuruk1 = (Elektronik) HPBuruk;
        Barang ModemMenengah = new Elektronik("Modem", "Menengah");
        Elektronik ModemMenengah1 = (Elektronik) ModemMenengah;
        Sierra.tambahBarang(HPBuruk1);
        Sierra.tambahBarang(ModemMenengah1);
        assertEquals("Kardus Elektronik: Terdapat 2 barang elektronik dan 0 pakaian", (Sierra.rekap()));
    }

    @Test
    public void rekapPakaian(){
        Kardus <Pakaian> Alpha = new Kardus<Pakaian>();
        Barang LBiru = new Pakaian('L', "Biru");
        Pakaian LBiru1 = (Pakaian) LBiru;
        Barang MMerah = new Pakaian('L', "Merah");
        Pakaian MMerah1 = (Pakaian) MMerah;
        Alpha.tambahBarang(LBiru1);
        Alpha.tambahBarang(MMerah1);
        assertEquals("Kardus Pakaian: Terdapat 0 barang elektronik dan 2 pakaian", (Alpha.rekap()));
    }

    @Test
    public void rekapCampuran(){
        Kardus <Barang> Delta = new Kardus<Barang>();
        Barang LBiru = new Pakaian('L', "Biru");
        Pakaian LBiru1 = (Pakaian) LBiru;
        Barang HPBuruk = new Elektronik("HP", "Buruk");
        Elektronik HPBuruk1 = (Elektronik) HPBuruk;
        Delta.tambahBarang(LBiru1);
        Delta.tambahBarang(HPBuruk1);
        assertEquals("Kardus Campuran: Terdapat 1 barang elektronik dan 1 pakaian", (Delta.rekap()));
    }

    @Test
    public void UkuranPakaian(){
        Barang LBiru = new Pakaian('L', "Biru");
        Pakaian LBiru1 = (Pakaian) LBiru;
        assertEquals('L', LBiru1.getUkuran());
    }

    @Test
    public void WarnaPakaian(){
        Barang LBiru = new Pakaian('L', "Biru");
        Pakaian LBiru1 = (Pakaian) LBiru;
        assertEquals("Biru", LBiru1.getWarna());
    }

    @Test
    public void ValuePakaian(){
        Barang LBiru = new Pakaian('L', "Biru");
        Pakaian LBiru1 = (Pakaian) LBiru;
        assertEquals("40.0", Double.toString(LBiru1.getValue()));
    }

    @Test
    public void ValueElektronik(){
        Barang HPBuruk = new Elektronik("HP", "Buruk");
        Elektronik HPBuruk1 = (Elektronik) HPBuruk;
        assertEquals("50.0", Double.toString(HPBuruk1.getValue()));
    }

    @Test
    public void JenisElektronik(){
        Barang HPBuruk = new Elektronik("HP", "Buruk");
        Elektronik HPBuruk1 = (Elektronik) HPBuruk;
        assertEquals("HP", HPBuruk1.getJenis());
    }

    @Test
    public void KondisiElektronik(){
        Barang HPBuruk = new Elektronik("HP", "Buruk");
        Elektronik HPBuruk1 = (Elektronik) HPBuruk;
        assertEquals("Buruk", HPBuruk1.getKondisi());
    }

    @Test
    public void tambahBarang(){
        Kardus <Barang> Delta = new Kardus<Barang>();
        Barang HPBuruk = new Elektronik("HP", "Buruk");
        Elektronik HPBuruk1 = (Elektronik) HPBuruk;
        Barang LBiru = new Pakaian('L', "Biru");
        Pakaian LBiru1 = (Pakaian) LBiru;
        Delta.tambahBarang(HPBuruk1);
        Delta.tambahBarang(LBiru1);
        assertEquals("[HP Buruk, L Biru]", Delta.getBarang().toString());
    }

}