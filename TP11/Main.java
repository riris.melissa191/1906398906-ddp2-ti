import java.util.*;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        Kardus <Elektronik> Sierra = new Kardus<Elektronik>();
        Kardus <Pakaian> Delta = new Kardus<Pakaian>();
        Kardus <Barang> Alpha = new Kardus<Barang>();

        System.out.println("Selamat datang di layanan donasi Desa Dedepe Dua!");
        System.out.println("Layanan disponsori oleh Kedai VoidMain");

        int jumlahElektronik = 0;
        int jumlahPakaian = 0;
        int jumlahCampuran = 0;

        try {
            System.out.println("Ada berapa jumlah barang untuk elektronik, pakaian, dan campuran?: ");
            String masukan = sc.nextLine();
            String[] masukanSplit = masukan.split(" ");
            jumlahElektronik = Integer.parseInt(masukanSplit[0]);
            jumlahPakaian = Integer.parseInt(masukanSplit[1]);
            jumlahCampuran = Integer.parseInt(masukanSplit[2]); 
        } catch (NumberFormatException e) {
            //TODO: handle exception
            System.out.println("Maaf harus angka yah!");
        } catch (ArrayIndexOutOfBoundsException e){
            System.out.println("Ga lengkap nih datanya!");
        }

        System.out.println("Silahkan masukan keterangan barang elektronik");
        System.out.println("Dengan format 'jenis kondisi' tanpa tanda ' ");
        for (int i=0; i<jumlahElektronik; i++){
            String masukanElektronik = sc.nextLine();
            String[] masukanSplit = masukanElektronik.split(" ");
            if ((masukanSplit[0].equalsIgnoreCase("hp") || masukanSplit[0].equalsIgnoreCase("laptop") || masukanSplit[0].equalsIgnoreCase("modem"))){
                if ((masukanSplit[1].equalsIgnoreCase("baru") || masukanSplit[1].equalsIgnoreCase("baik") || masukanSplit[1].equalsIgnoreCase("menengah") || masukanSplit[1].equalsIgnoreCase("buruk"))){
                    Elektronik elektronik = new Elektronik(masukanSplit[0],masukanSplit[1]);
                    Sierra.tambahBarang(elektronik);
                }
                else{
                    System.out.println("Kondisi barangnya salah ini.");
                }
            }
            else{
                System.out.println("Jenis barangnya salah ini.");
            }
        }

        System.out.println("Silahkan masukan keterangan barang pakaian");
        System.out.println("Dengan format 'ukuran warna' tanpa tanda ' ");
        for (int i=0; i<jumlahPakaian; i++){
            String masukanPakaian = sc.nextLine();
            String[] masukanSplit = masukanPakaian.split(" ");
            char ukuran = masukanSplit[0].charAt(0);
            if ((((ukuran+"").equalsIgnoreCase("L") || (ukuran+"").equalsIgnoreCase("M") || (ukuran+"").equalsIgnoreCase("S")))){
                Pakaian pakaian = new Pakaian(ukuran,masukanSplit[1]);
                Delta.tambahBarang(pakaian);
            }
            else{
                System.out.println("Ukurannya barangnya salah ini.");
            }
        }

        System.out.println("Silahkan masukan keterangan barang campuran");
        System.out.println("Dengan format 'ELEKTRONIK jenis kondisi' tanpa tanda '");
        System.out.println("atau 'PAKAIAN ukuran warna' untuk pakaian");

        for (int i=0; i<jumlahCampuran; i++){
            String masukanCampuran = sc.nextLine();
            String[] masukanSplit = masukanCampuran.split(" ");
            if (masukanSplit[0].equalsIgnoreCase("elektronik")){
                if ((masukanSplit[1].equalsIgnoreCase("hp") || masukanSplit[1].equalsIgnoreCase("laptop") || masukanSplit[1].equalsIgnoreCase("modem"))){
                    if ((masukanSplit[2].equalsIgnoreCase("baru") || masukanSplit[2].equalsIgnoreCase("baik") || masukanSplit[2].equalsIgnoreCase("menengah") || masukanSplit[2].equalsIgnoreCase("buruk"))){
                        Elektronik elektronik = new Elektronik(masukanSplit[1],masukanSplit[2]);
                        Alpha.tambahBarang(elektronik);
                    }
                    else{
                        System.out.println("Kondisi barangnya salah ini.");
                    }
                }
                else{
                    System.out.println("Jenis barangnya salah ini.");
                }
            }
            else if (masukanSplit[0].equalsIgnoreCase("pakaian")){
                char ukuran = masukanSplit[1].charAt(0);
                if ((((ukuran+"").equalsIgnoreCase("L") || (ukuran+"").equalsIgnoreCase("M") || (ukuran+"").equalsIgnoreCase("S")))){
                    Pakaian pakaian = new Pakaian(ukuran,masukanSplit[2]);
                    Alpha.tambahBarang(pakaian);
                }
                else{
                    System.out.println("Ukurannya barangnya salah ini.");
                }
            }
            else{
                System.out.println("Mon maap bukan elektronik/pakaian");
            }
        }

        System.out.println("-----------------------------------");
        System.out.println("Donasi Anda sebesar ");
        System.out.println(Sierra.getTotalValue() + Delta.getTotalValue() + Alpha.getTotalValue() + "0 DDD");
        System.out.println("Rekap donasi untuk tiap kardus:");
        System.out.println(Sierra.rekap());
        System.out.println(Delta.rekap());
        System.out.println(Alpha.rekap());
        System.out.println("-----------------------------------");



        TabelPengiriman<Barang>tabelPengiriman = new TabelPengiriman<Barang>(new Barang[3][4]);
        for (int k=0 ; k<Sierra.getBarang().size(); k++){
            tabelPengiriman.tambahBarang(Sierra.getBarang().get(k), 0);
        }
        for (int k=0 ; k<Delta.getBarang().size(); k++){
            tabelPengiriman.tambahBarang(Delta.getBarang().get(k), 1);
        }
        for (int k=0 ; k<Alpha.getBarang().size(); k++){
            tabelPengiriman.tambahBarang(Alpha.getBarang().get(k), 2);
        }
        System.out.println(tabelPengiriman.rekap());
    }
}