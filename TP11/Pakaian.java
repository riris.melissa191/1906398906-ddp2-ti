class Pakaian extends Barang {
    private char ukuran;
    private String warna;

    public Pakaian() {
    }

    public Pakaian(char ukuran, String warna) {
        this.ukuran = ukuran;
        this.warna = warna;
    }

    public char getUkuran() {
        return ukuran;
    }

    /** Set a new jenis */
    public void setUkuran(char ukuran) {
        this.ukuran = ukuran;
    }

    public String getWarna() {
        return warna;
    }

    /** Set a new kondisi */
    public void setWarna(String warna) {
        this.warna = warna;
    }
    
    public double getValue(){
        double value = 0.0;
        if (this.ukuran == 'l' || this.ukuran == 'L'){
            value = 40.0;
        }
        else if (this.ukuran == 'm' || this.ukuran == 'M'){
            value = 35.0;
        }
        else if (this.ukuran == 's' || this.ukuran == 'S'){
            value = 30.0;
        }
        return value;
    }

    public String toString() {
        return this.ukuran + " " + this.warna;
    }
}