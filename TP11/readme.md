# 💻 Tugas Pemrograman 11 (Individu)

[![License](https://img.shields.io/badge/License-BSD%203--Clause-blue.svg)](https://opensource.org/licenses/BSD-3-Clause) [![](https://img.shields.io/badge/SCeLE-Click%20Here-orange.svg)](https://scele.cs.ui.ac.id/course/view.php?id=3019) [![](https://img.shields.io/badge/LINE%20OA-Kak%20Burhan-brightgreen)](https://line.me/R/ti/p/%40157zmdwf)

**Nama**: Riris Melissa Winona Togatorop | **NPM**: 1906398906 | **Kelas**: J | **Kode Tutor**: MP

## Topik

We’ve learned this kind of problem before, why generics?

## Dokumen Tugas

TBA

## *Checklist*

- [X] TBA
- [X] TBA
- [X] TBA
- [X] TBA