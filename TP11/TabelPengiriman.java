import java.util.*;

class TabelPengiriman <T extends Barang>{
    private T[][] isiTabel;

    public TabelPengiriman(T[][] barang) {
        this.isiTabel=barang;
    }
    

    @SuppressWarnings("unchecked")

	public void tambahBarang(Barang barang, int mitraID) {
        if (barang instanceof Elektronik){
            Elektronik elektronik = (Elektronik) barang;
            if (elektronik.getKondisi().equalsIgnoreCase("baru")){
                isiTabel[mitraID][0] = (T) elektronik;
            }
            else if (elektronik.getKondisi().equalsIgnoreCase("baik")){
                isiTabel[mitraID][1] = (T) elektronik;
            }
            else if (elektronik.getKondisi().equalsIgnoreCase("menengah")){
                isiTabel[mitraID][2] = (T) elektronik;
            }
            else if (elektronik.getKondisi().equalsIgnoreCase("buruk")){
                isiTabel[mitraID][3] = (T) elektronik;
            }
        }

        else if (barang instanceof Pakaian){
            Pakaian pakaian = (Pakaian) barang;
            if ((pakaian.getUkuran()+"").equalsIgnoreCase("L")){
                isiTabel[mitraID][0] = (T) pakaian;
            }
            else if ((pakaian.getUkuran()+"").equalsIgnoreCase("M")){
                isiTabel[mitraID][1] = (T) pakaian;
            }
            else if ((pakaian.getUkuran()+"").equalsIgnoreCase("S")){
                isiTabel[mitraID][2] = (T) pakaian;
            }
        }
    }

    public String rekap(){
        String pesan = "";
        for (int i=0; i<3;i++){
            if (i==0){
                pesan += "Sierra: ";
            }
            else if (i==1){
                pesan += "Delta: ";
            }
            else if (i==2){
                pesan += "Alpha: ";
            }
            for (int j=0; j<4; j++){
                if (this.isiTabel[i][j] != null){
                    pesan += this.isiTabel[i][j].toString() + "/";
                }
                else{
                    pesan += "None/";
                }
            }
            pesan += "\n";
        }
        return pesan;
    }
}