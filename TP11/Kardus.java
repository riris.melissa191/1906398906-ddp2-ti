import java.util.*;

class Kardus <T extends Barang>{
    private ArrayList <T> barang;
    
    public Kardus() {
        this.barang = new ArrayList <T> ();
        }
        
    public ArrayList<T> getBarang() {
        return barang;
        }

    String rekap(){
        int totalE = 0;
        int totalP = 0;
        String namakardus = "";
        for (int i=0; i<this.barang.size(); i++){
            try {
                Elektronik elektronik = (Elektronik) this.barang.get(i);
                totalE +=1;
            } catch (ClassCastException e) {
            }
            try{
                Pakaian pakaian = (Pakaian) this.barang.get(i);
                totalP +=1;
            } catch (ClassCastException e) {
            }
            if (totalE==0){
                namakardus = "Kardus Pakaian";
            }
            else if (totalP==0){
                namakardus = "Kardus Elektronik";
            }
            else{
                namakardus = "Kardus Campuran";
            }
        }
        return namakardus + ":" + " Terdapat " + totalE + " barang elektronik dan " + totalP + " pakaian";
    }

    double getTotalValue(){
        double total = 0;
        for (int i=0; i<barang.size(); i++){
            total+= barang.get(i).getValue();
        }
        return total;
    }

    void tambahBarang(T barang){
        this.barang.add(barang);
    }

    public String toString() {
        String res = "";
        for (int i=0; i>this.barang.size() ; i++){
            res += this.barang.get(i) + " ";
        }
        return res;
    }

}